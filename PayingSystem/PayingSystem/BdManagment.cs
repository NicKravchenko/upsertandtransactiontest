﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayingSystem
{
    internal class BdManagment
    {
        private static SqlConnection Connection = new SqlConnection();
        private static SqlTransaction transaction = null;

        static void OpenConnection()
        {        
            Connection.ConnectionString = @"Data Source=.\SQLEXPRESS;AttachDbFilename=C:\Users\Nikita\Desktop\Projects\IDS345\PayingSystem\PayingSystem\MyDatabase.mdf;Integrated Security=True;User Instance=True";
            Connection.Open();            
        }
        static void CloseConnection()
        {
            Connection.Close();
        }

        public void Upsert()
        {
            string nombre;
            string cedula;
            float balance;

            OpenConnection();

            Console.WriteLine("Nombre:");
            nombre = Console.ReadLine();

            Console.WriteLine("Cedula:");
            cedula = Console.ReadLine();

            Console.WriteLine("Balance:");
            balance = float.Parse(Console.ReadLine());

            SqlCommand cmd = Connection.CreateCommand();

            cmd.Connection = Connection;
            cmd.CommandText = "ppUpsertCliente";
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@nombre", nombre);
            cmd.Parameters.AddWithValue("@cedula", cedula);
            cmd.Parameters.AddWithValue("@balance", balance);

            cmd.ExecuteNonQuery();

            CloseConnection();
        }

        public void Transferir()
        {
            int IdOrigen;
            int IdDestino;
            float monto;

            Console.WriteLine("IdOrigen:");
            IdOrigen = int.Parse(Console.ReadLine());

            Console.WriteLine("IdDestino:");
            IdDestino = int.Parse(Console.ReadLine());

            Console.WriteLine("Monto:");
            monto = float.Parse(Console.ReadLine());

            OpenConnection();

            try
            {
                SqlCommand cmd = Connection.CreateCommand();
                cmd.Connection = Connection;

                transaction = Connection.BeginTransaction();

                cmd.CommandText = "ppHacerTransferencia";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                cmd.Transaction = transaction;

                cmd.Parameters.AddWithValue("@IdOrigen", IdOrigen);
                cmd.Parameters.AddWithValue("@IdDestino", IdDestino);
                cmd.Parameters.AddWithValue("@Monto", monto);

                cmd.ExecuteNonQuery();

                MakeOperation(IdOrigen, -monto, transaction);

                MakeOperation(IdDestino, monto, transaction);

                transaction.Commit();

            }
            catch (Exception ex)
            {
                transaction.Rollback();
                Console.WriteLine("Error en transaccion");

            }


            CloseConnection();
        }

        private void MakeOperation(int idCliente, float monto, SqlTransaction transaction)
        {
            SqlCommand cmd = Connection.CreateCommand();
            cmd.Connection = Connection;

            cmd.Transaction = transaction;

            cmd.CommandText = "ppMakeTransfer";
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@IdCliente", idCliente);
            cmd.Parameters.AddWithValue("@Monto", monto);

            cmd.ExecuteNonQuery();

        }
    }
}
